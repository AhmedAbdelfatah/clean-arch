enum Status {
  OnProgress = 'OnProgress',
  OnHold = 'OnHold',
  Done = 'Done'
}
type ToDoProps = {
  id: string;
  user: string;
  title: string;
  description: string;
  status: Status;
  createdAt: number;
  updatedAt: number;
};
export default class Todo {
  public id: string;
  public user: string;
  public title: string;
  public description: string;
  public status: Status;
  public createdAt: number;
  public updatedAt: number;

  constructor({
    id,
    user,
    title,
    description,
    status,
    createdAt,
    updatedAt
  }: ToDoProps) {
    this.id = id;
    this.createdAt = createdAt;
    this.description = description;
    this.status = status;
    this.title = title;
    this.user = user;
    this.updatedAt = updatedAt;
  }
}
