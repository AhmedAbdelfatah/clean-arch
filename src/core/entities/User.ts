type UserProps = {
  id: string;
  name: string;
  email: string;
  password: string;
  createdAt: number;
  updatedAt: number;
};

export default class User {
  public id: string;
  public name: string;
  public email: string;
  public password: string;
  public createdAt: number;
  public updatedAt: number;

  constructor({ id, name, email, password, createdAt, updatedAt }: UserProps) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
  }
}
